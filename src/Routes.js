import React, {lazy, Suspense} from 'react'
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import Home from './core/Home';
import Menu from './core/Menu';
import AddCart from './core/AddCart';
import GotoCart from './core/GotoCart';
const Routes = () => {
    return(
     
        <BrowserRouter>

                <Menu />
                <Switch>
                <Route path='/' component={Home} exact />
                {/* <Route path='/addcart' component={AddCart} exact /> */}
                <Route path='/gotocart' component={GotoCart} exact />
                {/* <Route path='/cart' component={Cart} /> */}
                </Switch>
        </BrowserRouter>
    )
}

export default Routes;