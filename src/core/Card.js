import React, {Fragment, useState} from 'react';
import {addItem} from './CartHelper';
import {Redirect} from 'react-router-dom'


const Card = ({product}) =>{

            const [redirect, setRedirect] = useState(false)

        const addToCart = () =>{
            addItem(product, () =>{
               setRedirect(true)
            })
        }

       const shouldRedirect = (redirect) =>{
           if(redirect){
               return <Redirect to='/gotocart' />
           }
       }

      

        const showAddCart = () =>(

                
                <button onClick={addToCart} 
                className='btn btn-outline-primary mt-2 mb-2'>
                Add to card
                </button>
                
        
        )


    return(
        <Fragment>
          
            <div className='card'>
                <div className='card-header'>{product.name}</div>
                <div className='card-body'>
                {shouldRedirect(redirect)}
                    <p>{product.description}</p>
                    <p>${product.price}</p>
                    <br />
                        {showAddCart()}
                </div>
            </div>
            
            </Fragment>
    )
}


export default Card;