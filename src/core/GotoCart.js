import React, {useState, useEffect} from 'react';
import Layout from './Layout';
import {getCart} from './CartHelper'
import AddCart from './AddCart';
import {Link} from 'react-router-dom'
import Card from './Card';
import Order from './Order';



const GotoCart = () =>{

    const [items, setItems] = useState([])



    useEffect(()=>{
        setItems(getCart())
    }, [])

    const showItems = (items) =>{

        return(
            <div>
            <h2>your cart has {`${items.length}`} items</h2>
                    <hr />
                    {items.map((product, i) => (
                        <AddCart key={i} 
                        product={product}
                        cartUpdate={true}
                        />
                        
                    ))}
            </div>
        )
    }

    const noItemMessage = () =>(
        <h2>your cart is empty <br /> <Link to='/'>Continue shopping</Link></h2>
    )



    return(

        <Layout title='Order Title' description='Order page'
                classname='container-fluid'>
            <div className='row'>
                <div className='col-6'>
                    {items.length > 0 ? showItems(items) : noItemMessage()}
                </div>

                <div className='col-6'>
                    <h2>total sumary</h2>
                        <hr />
                            <Order product={items}/>
                </div>
                
            </div>
           
        </Layout>
    )

}

export default GotoCart;