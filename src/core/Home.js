import React, {useState, useEffect, Fragment} from 'react';
import Layout from './Layout';
import {getCategories} from './Apis';
import Card from './Card';
const Home = () =>{

   const [products, setProduct] = useState([]);
   const [error, setError]=useState([]);

    const init = () =>{
        getCategories().then(data =>{
            if(data.error){
                setError(data.error)
            } else {
                setProduct(data);
            }
        })
    }

    useEffect( () =>{
        init();
    }, [])

    return(
    <Layout title='home page' description='product page'>
       <div>
       <h4>all product items</h4>
       </div>
       <div className='row'>
            {products.map((product, i)=>(
                <div>
                <Card key={i} product={product} />
                </div>
            ))}
            </div>
    
    </Layout>
   
    )
}

export default Home;