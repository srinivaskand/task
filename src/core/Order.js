import React from 'react';
import Layout from './Layout';
import {totalPrice} from './CartHelper'


const Order = ({product}) =>{

    const getTotal = () =>{

        return product.reduce((currentValue, nextValue) =>{
            return currentValue + nextValue.count * nextValue.price;
        }, 0)
    };
    return(
 
        <div>
           <h2>Total:${getTotal()}</h2>
                    
      </div>     
    )
}       

export default Order;