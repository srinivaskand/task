import React from 'react'
const Layout = ({
    title ='Title', 
    description = 'Description', 
    classname,
    children
}) =>(
    <div>
    <div className='jumbotron'>
      <h2>{title}</h2>
      <p>{description}</p>
    </div>
    <div className={classname}>{children}</div>
    </div>
)

export default Layout;