import React from 'react';
import {Link, withRouter} from 'react-router-dom'
import {totalItem} from './CartHelper'

const isActive = (history, path) =>{
    if(history.location.pathname === path){
        return {color: '#ff9900'};
    } else {
        return {color: '#ffffff'};
    }
}

const Menu = ({ history }) =>(
    <div>
        <ul className='nav nav-tabs bg-primary'>
        {/* <li className='nav-item'>
        <Link className='nav-link' 
          style={isActive(history, "/addcart")}
          to='/addcart'>AddCart</Link>
        </li> */}
        <li className='nav-item'>
        <Link className='nav-link' 
          style={isActive(history, "/gotocart")}
          to='/gotocart'>GotoCart <sup><small className='cart-badge'>{totalItem()}</small></sup></Link>
        </li>
        <li className='nav-item'>
        <Link className='nav-link' 
          style={isActive(history, "/")}
          to='/'>Home</Link>
        </li>
        </ul>
    </div>
)

export default withRouter(Menu);