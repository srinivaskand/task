import React, {useState, useEffect} from 'react';
import Layout from './Layout';
import {updateItem} from './CartHelper'


const AddCart = ({product, cartUpdate=false}) =>{

    const [count, setCount] = useState(product.count);
  

const handleChange = productId =>event =>{
        setCount(event.target.value < 1 ? 1 : event.target.value)
        if(event.target.value >= 1){
            updateItem(productId, event.target.value)
        }
}



    const cartUpdateOptional = (cartUpdate) =>{
        return cartUpdate && <div>
            <div className='input-group mb-3'>
                <div className='input-group-prepend'>
                    <span className='input-group-text'>incr/decr</span>

                </div>
                <input type='number' className='form-control' value={count} 
                 onChange={handleChange(product.id)}
                 />
            </div>
        </div>
       }


    return(
 
        <div>
            <table width='100%'>
                            <tr>
                                <th>id</th>
                                <th>item</th>
                                <th>price</th>
                            </tr>
                            <tr>
                                <td>{product.id}</td>
                                <td>{cartUpdateOptional(cartUpdate)}</td>
                                <td>${product.price}</td>
                            </tr>
                            
</table>

      </div>     
    )
}       

export default AddCart;